package com.pligor.vetapp_antonopoulos_tsoupa;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.pinapps.vetapp.R;
import com.pligor.pet_crud.DatabaseHandler;

public class MenuListActivity extends ListActivity {

	String classes[] = {
			"SplashActivity",
			"AboutActivity",
			"GoogleMapActivity",
			"AppointmentFormActivity",
			"StaticContentActivity",
			"PetListActivity",
			"PetDeleteListActivity",
			"PetFormActivity",
			"RssLoadingActivity",
			"StaticContentActivity",
	};

	private String packageName;

	private void testing() {
		boolean check;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		DatabaseHandler dbHandler = new DatabaseHandler(this.getApplicationContext());
		// dbHandler.reset(); // TODO keep only for the debugging period

		// must be executed before setting content layout or the list adapter in our case
		// FULLSCREEN
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		// here we don't have a content view
		this.setListAdapter(new ArrayAdapter<String>(MenuListActivity.this, android.R.layout.simple_list_item_1, this.classes));
		// USE ArrayAdapter ONLY with TextViews !!!

		this.packageName = MenuListActivity.class.getPackage().getName();

		this.testing();
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);

		String className = classes[position];
		try {
			String fullClassName = this.packageName + "." + className;
			Class curClass = Class.forName(fullClassName);
			Intent ourIntent = new Intent(MenuListActivity.this, curClass);
			startActivity(ourIntent);
		}
		catch (ClassNotFoundException e) {
			e.printStackTrace();
			Log.i("pl", "the class " + className + " was not found");
		}

	}

	@Override
	public boolean onCreateOptionsMenu(android.view.Menu menu) {
		boolean created = super.onCreateOptionsMenu(menu);
		// this.getMenuInflater().inflate(R.menu.cool_menu, menu);
		return created;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean selected = super.onOptionsItemSelected(item);

		int item_id = item.getItemId();

		return selected;
	}

}
