package com.pligor.vetapp_antonopoulos_tsoupa;

import android.app.Application;
import android.content.Context;
import com.pinapps.vetapp.R;

public class Veterinary extends Application {
	private static Context context;

	@Override
	public void onCreate() {
		super.onCreate();
		Veterinary.context = this.getApplicationContext();
	}

	public static Context getAppContext() {
		return Veterinary.context;
	}
}
