package com.pligor.vetapp_antonopoulos_tsoupa;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;

import com.pinapps.vetapp.R;
import com.pligor.generic.StaticTools;

public class StaticContentActivity extends bbActivity implements OnClickListener {

	WebView staticContentWebView;

	private AlertDialog callDialog;

	private static final String vet_url = "http://www.animapps.gr/main/veterinary-grafeio.html";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.initViews();

		this.initWebView();

		this.initCallDialog();
	}

	private void initCallDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.call_dialog_title);
		String message = this.getResources().getString(R.string.call_dialog_message) + " " + AboutActivity.LANDPHONE;
		builder.setMessage(message);
		builder.setPositiveButton(R.string.call_dialog_positive_button, this);
		builder.setNegativeButton(R.string.call_dialog_negative_button, null);

		this.callDialog = builder.create();
	}

	private void initWebView() {
		try {
			this.staticContentWebView.loadUrl(StaticContentActivity.vet_url);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void initViews() {
		this.setContentView(R.layout.activity_static_content);

		this.staticContentWebView = (WebView) this.findViewById(R.id.staticContentWebView);
	}

	public void onPhoneImageButtonClick(View view) {
		this.callDialog.show();
	}

	public void onMailImageButtonClick(View view) {
		String email = this.getResources().getString(R.string.about_email);
		String subject = "Veterinary App";
		String body = "";
		StaticTools.sendEmail(this, email, subject, body);
	}

	public void onBackClick(View view) {
		this.finish();
	}

	public void onClick(DialogInterface dialog, int which) {
		// we do not check which dialog this is but never mind we only got one dialog
		if(which == AlertDialog.BUTTON_POSITIVE) {
			StaticTools.phoneCall(this, AboutActivity.LANDPHONE);
		}
	}
}
