package com.pligor.vetapp_antonopoulos_tsoupa;

import java.util.ArrayList;
import java.util.List;

import android.graphics.drawable.Drawable;

import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.OverlayItem;
import com.pinapps.vetapp.R;

/**
 * BE AWARE: An Overlay need at least one item before adding it to map view !!!!
 * 
 * @author pligor
 * 
 */
public class MyItemizedOverlay extends ItemizedOverlay {

	protected List<OverlayItem> items = new ArrayList<OverlayItem>();

	public MyItemizedOverlay(Drawable defaultMarker) {
		// Adjusts a drawable's bounds so that (0,0) is a pixel in the center of the bottom row of the drawable.
		super(ItemizedOverlay.boundCenterBottom(defaultMarker));
	}

	public void addItem(OverlayItem overlayItem) {
		this.items.add(overlayItem);

		this.populate(); // this is crucial to render items
	}

	public void removeItem(OverlayItem overlayItem) {
		this.items.remove(overlayItem);

		this.populate(); // this is crucial to render items
	}

	public void clearItems() {
		this.items.clear();

		this.populate(); // this is crucial to render items
	}

	@Override
	protected OverlayItem createItem(int i) {
		return this.items.get(i);
	}

	@Override
	public int size() {
		return this.items.size();
	}
}
