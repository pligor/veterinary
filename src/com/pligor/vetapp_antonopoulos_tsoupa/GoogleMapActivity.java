package com.pligor.vetapp_antonopoulos_tsoupa;

import java.util.List;

import android.graphics.drawable.Drawable;
import android.os.Bundle;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;
import com.pinapps.vetapp.R;
import com.pligor.generic.MapHelper;

public class GoogleMapActivity extends MapActivity {

	public List<Overlay> mapOverlays;

	private MapView googleMapView;

	@Override
	protected void onCreate(Bundle icicle) {
		super.onCreate(icicle);

		this.initViews();

		this.initMap();
	}

	private void initMap() {
		this.mapOverlays = this.googleMapView.getOverlays();

		Drawable drawable = this.getResources().getDrawable(R.drawable.simple_pin);

		MyItemizedOverlay overlay = new MyItemizedOverlay(drawable);

		{
			GeoPoint point = new GeoPoint(MapHelper.degreesToInt(AboutActivity.first_lat), MapHelper.degreesToInt(AboutActivity.first_lon));
			OverlayItem overlayItem = new OverlayItem(point, "title", "snippet");
			overlay.addItem(overlayItem);
		}

		{
			GeoPoint point = new GeoPoint(MapHelper.degreesToInt(AboutActivity.second_lat), MapHelper.degreesToInt(AboutActivity.second_lon));
			OverlayItem overlayItem = new OverlayItem(point, "title", "snippet");
			overlay.addItem(overlayItem);
		}

		this.mapOverlays.add(overlay);

		float half_lat = (AboutActivity.first_lat + AboutActivity.second_lat) / 2;
		float half_lon = (AboutActivity.first_lon + AboutActivity.second_lon) / 2;
		GeoPoint point = new GeoPoint(MapHelper.degreesToInt(half_lat), MapHelper.degreesToInt(half_lon));
		this.googleMapView.getController().setCenter(point);
	}

	private void initViews() {
		this.setContentView(R.layout.activity_google_map);

		this.googleMapView = (MapView) this.findViewById(R.id.googleMapView);

		this.googleMapView.setBuiltInZoomControls(true);
		this.googleMapView.setClickable(true);
	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

}
