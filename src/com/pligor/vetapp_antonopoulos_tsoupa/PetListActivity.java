package com.pligor.vetapp_antonopoulos_tsoupa;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import com.pinapps.vetapp.R;
import com.pligor.pet_crud.DatabaseHandler;

public class PetListActivity extends bbActivity implements OnItemClickListener {

	private ListView petListView;
	private PetListAdapter petListAdapter;
	private Button editPetButton;
	private ImageView petInfoStartImageView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.initViews();

		this.initListeners();

		this.petListAdapter = new PetListAdapter(this);
		this.petListView.setAdapter(this.petListAdapter);
	}

	private void initListeners() {
		this.petListView.setOnItemClickListener(this);
	}

	@Override
	protected void onPause() {
		super.onPause();

		this.withItemsCase(); // reset visibility
	}

	@Override
	protected void onResume() {
		super.onResume();

		this.initList();
	}

	private void initViews() {
		this.setContentView(R.layout.activity_pet_list);

		this.petListView = (ListView) this.findViewById(R.id.petListView);
		this.editPetButton = (Button) this.findViewById(R.id.editPetButton);

		this.petInfoStartImageView = (ImageView) this.findViewById(R.id.petInfoStartImageView);
	}

	private void initList() {
		DatabaseHandler dbHandler = new DatabaseHandler(this.getApplicationContext());
		List items = dbHandler.getAll();
		this.petListAdapter.setItems(items);
		// Log.i("pl", "just set all the items");

		if(items == null) {
			this.noItemsCase();
		}
		else if(items.isEmpty()) {
			this.noItemsCase();
		}
	}

	private void noItemsCase() {
		this.editPetButton.setVisibility(View.GONE);
		this.petListView.setVisibility(View.GONE);
		this.petInfoStartImageView.setVisibility(View.VISIBLE);
	}

	private void withItemsCase() {
		this.petInfoStartImageView.setVisibility(View.GONE);
		this.petListView.setVisibility(View.VISIBLE);
		this.editPetButton.setVisibility(View.VISIBLE);
	}

	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Intent intent = new Intent(this, PetFormActivity.class);
		intent.putExtra(PetFormActivity.EXTRA_PET_ID, (int) id);
		this.startActivity(intent);
	}

	public void onAddClick(View view) {
		Intent intent = new Intent(this, PetFormActivity.class);
		// no extras!
		this.startActivity(intent);
	}

	public void onEditClick(View view) {
		Intent intent = new Intent(this, PetDeleteListActivity.class);
		this.startActivity(intent);
	}

	/*
	 * public void test_list(View view) { this.initList(); }
	 */
}
