package com.pligor.vetapp_antonopoulos_tsoupa;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.pinapps.vetapp.R;
import com.pligor.blogspot.RssListActivity;
import com.pligor.generic.InternetFetcher;

public class RssLoadingActivity extends bbActivity {

	private static final String PROMPT_STRING = "Ενημέρωση RSS";
	private static final String RSS_URL_STRING = "http://www.veterinary-gr.blogspot.com/feeds/posts/default?alt=rss";

	public static final String EXTRA_RSS_STRING = "raw_rss";

	private AlertDialog alertDialog;

	private FetchRss fetchRss = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.alertDialog = this.buildSimpleDialog();
		this.configSimpleDialog();

		this.initViews();
	}

	@Override
	protected void onPause() {
		super.onPause();

		if(this.fetchRss != null) {
			this.fetchRss.cancel(true); // true mayInterruptIfRunning
		}
		this.fetchRss = null;
	}

	@Override
	protected void onResume() {
		super.onResume();

		this.fetchRss = new FetchRss();
		this.fetchRss.execute(RssLoadingActivity.RSS_URL_STRING);
	}

	private void initViews() {
		this.setContentView(R.layout.activity_rss_loading);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if(resultCode != Activity.RESULT_OK) { // on error
			this.finish();
		}
	}

	/**
	 * Params, Progress, Result
	 * 
	 * @author pligor
	 */
	private class FetchRss extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			RssLoadingActivity.this.alertDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				Thread.sleep(1000); // msec
			}
			catch (Exception e) {
			}

			String urlString = params[0];

			return new InternetFetcher().getData(urlString);
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			RssLoadingActivity.this.alertDialog.dismiss();

			if(result == null) {
				// getBaseContext helps to not have the toast disappear as long as the rssLoadingActivity finishes but remain active (for the
				// Toast.LENGTH_LONG period)
				Toast toast = Toast.makeText(RssLoadingActivity.this.getBaseContext(), "Δεν υπάρχει σύνδεση", Toast.LENGTH_LONG);
				toast.show();
				RssLoadingActivity.this.finish();
			}
			else {
				// Log.i("pl", result);
				Intent intent = new Intent(RssLoadingActivity.this, RssListActivity.class);
				intent.putExtra(RssLoadingActivity.EXTRA_RSS_STRING, result);
				RssLoadingActivity.this.startActivityForResult(intent, 0); // we do not care for the request code for the time being

				// because when we were on item list, by clicking back, the list activity had already been lost!
				// RssLoadingActivity.this.startActivity(intent);
				// RssLoadingActivity.this.finish();
			}
		}
	}

	/**
	 * BE AWARE: use before setting content view. http://stackoverflow.com/questions/5469005/show-alertdialog-in-any-position-of-the-screen
	 */
	private void configSimpleDialog() {
		this.alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		WindowManager.LayoutParams wmlParams = this.alertDialog.getWindow().getAttributes();
		// wmlParams.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;
		wmlParams.gravity = Gravity.CENTER;
		wmlParams.y = -60;

		this.alertDialog.getWindow().setAttributes(wmlParams);
	}

	private AlertDialog buildSimpleDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		builder.setMessage(RssLoadingActivity.PROMPT_STRING);

		// builder.setTitle(RssLoadingActivity.PROMPT_STRING);

		builder.setCancelable(true);

		// builder.setPositiveButton(/*...*/);
		// builder.setNegativeButton(/*...*/);
		// builder.setNeutralButton(/*...*/);
		// builder.setIcon(R.drawable.ic_launcher);

		return builder.create();
	}
}
