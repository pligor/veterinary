package com.pligor.vetapp_antonopoulos_tsoupa;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ListView;

import com.pinapps.vetapp.R;
import com.pligor.pet_crud.DatabaseHandler;

public class PetDeleteListActivity extends bbActivity {

	private ListView petListView;

	private PetDeleteListAdapter petListAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE); // must be set BEFORE adding content

		this.initViews();

		this.petListAdapter = new PetDeleteListAdapter(this);
		this.petListView.setAdapter(this.petListAdapter);
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();

		this.initList();
	}

	private void initViews() {
		this.setContentView(R.layout.activity_pet_delete_list);

		this.petListView = (ListView) this.findViewById(R.id.petListView);
	}

	private void initList() {
		DatabaseHandler dbHandler = new DatabaseHandler(this.getApplicationContext());
		List items = dbHandler.getAll();
		this.petListAdapter.setItems(items);
	}

	public void onAddClick(View view) {
		Intent intent = new Intent(this, PetFormActivity.class);
		// no extras!
		this.startActivity(intent);
	}

	public void onDoneClick(View view) {
		this.finish();
	}
}
