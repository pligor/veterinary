package com.pligor.vetapp_antonopoulos_tsoupa;

import java.io.FileNotFoundException;
import java.io.IOException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.pinapps.vetapp.R;
import com.pligor.generic.StaticTools;
import com.pligor.pet_crud.DatabaseHandler;
import com.pligor.pet_crud.PetModel;

//"Don't use getBaseContext(), just use the Context you have." FROM: http://stackoverflow.com/questions/1026973/android-whats-the-difference-between-the-various-methods-to-get-a-context

public class PetFormActivity extends bbActivity implements OnClickListener {

	public static final String EXTRA_PET_ID = "pet_id";

	private PetModel model;

	private TextView titleTextView;
	private static final String DEFAULT_TITLE_STRING = "Κατοικίδιο";

	private Bundle inputBundle;

	private RadioGroup genderRadioGroup;
	private RadioButton maleRadioButton;
	private RadioButton femaleRadioButton;

	private EditText petFormPetNameEditText;
	private EditText petFormBirthdayEditText;
	private EditText petFormKindEditText;
	private EditText petFormBreedEditText;
	private EditText petFormChipCodeEditText;
	private EditText petFormVetNameEditText;
	private EditText petFormAddressEditText;
	private EditText petFormCellphoneEditText;
	private EditText petFormLandphoneEditText;
	private EditText petFormHomephoneEditText;
	private EditText petFormFaxEditText;
	private ImageView petFormPhotoImageView;

	private AlertDialog photoAlertDialog;

	private final static int CAMERA_DATA = 0;
	private final static int ACTIVITY_SELECT_IMAGE = 1;

	private Bitmap photoBitmap;

	// you cannot find the source of an intent. period! the best way to do this is by passing extras:
	// http://stackoverflow.com/questions/4789155/how-to-find-intent-source-in-android

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.initViews();
		this.setupPhotoDialog();

		Integer pet_id = null;
		try {
			pet_id = this.getIntent().getExtras().getInt(PetFormActivity.EXTRA_PET_ID);
		}
		catch (NullPointerException e) {
			// noop
		}

		// Log.i("pl", "in pet form act, pet id: " + String.valueOf(pet_id));

		if(pet_id == null) {
			this.model = new PetModel();
		}
		else {
			DatabaseHandler dbHandler = new DatabaseHandler(this.getApplicationContext());
			this.model = dbHandler.getById(pet_id);
			this.fill_form();
		}
	}

	private void setPhotoBitmap(Bitmap bitmap) {
		this.photoBitmap = bitmap;
		this.petFormPhotoImageView.setImageBitmap(this.photoBitmap);
	}

	private void setupPhotoDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.photo_dialog_title);
		builder.setCancelable(true);

		// builder.setNegativeButton(R.string.photo_dialog_cancel, this);

		this.photoAlertDialog = builder.create();

		this.photoAlertDialog.setButton(AlertDialog.BUTTON1, this.getResources().getString(R.string.photo_dialog_shoot), this);
		this.photoAlertDialog.setButton(AlertDialog.BUTTON3, this.getResources().getString(R.string.photo_dialog_select), this);
		// this.photoAlertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, this.getResources().getString(R.string.photo_dialog_cancel), this);
	}

	public void onClick(DialogInterface dialog, int which) {
		// we do not check which dialog it is but never mind :P we only have one dialog in this activity
		if(which == AlertDialog.BUTTON_NEGATIVE) {
			// nothing for now
		}
		else if(which == AlertDialog.BUTTON1) {
			Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
			startActivityForResult(intent, PetFormActivity.CAMERA_DATA);
		}
		else if(which == AlertDialog.BUTTON3) {
			Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
			this.startActivityForResult(intent, PetFormActivity.ACTIVITY_SELECT_IMAGE);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if(requestCode == PetFormActivity.CAMERA_DATA) {
			if(resultCode == Activity.RESULT_OK) {
				this.setPhotoBitmap((Bitmap) data.getExtras().get("data"));
			}
		}
		else if(requestCode == PetFormActivity.ACTIVITY_SELECT_IMAGE) {
			if(resultCode == Activity.RESULT_OK) {
				// COMBINED these two answers ;)
				// http://stackoverflow.com/questions/2507898/how-to-pick-a-image-from-gallery-sd-card-for-my-app-in-android
				// http://stackoverflow.com/questions/6687926/how-to-get-bitmap-providing-a-uri-java-io-filenotfoundexception-no-content-pro
				Uri imageUri = data.getData();
				try {
					Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
					this.setPhotoBitmap(bitmap);
				}
				catch (FileNotFoundException e) {
					e.printStackTrace();
				}
				catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void onPhotoClick(View view) {
		if(!StaticTools.isSDcardAvailable()) {
			Toast toast = Toast.makeText(this, R.string.sdcard_prompt, Toast.LENGTH_LONG);
			toast.show();
			return;
		}
		this.photoAlertDialog.show();
	}

	private void fill_form() {
		this.titleTextView.setText(this.model.getPet_name());

		this.petFormPetNameEditText.setText(this.model.getPet_name());
		this.setGender(this.model.getGender());
		this.petFormBirthdayEditText.setText(this.model.getBirthday());
		this.petFormKindEditText.setText(this.model.getKind());
		this.petFormBreedEditText.setText(this.model.getBreed());
		this.petFormChipCodeEditText.setText(String.valueOf(this.model.getChip_code()));
		this.petFormVetNameEditText.setText(this.model.getVet_name());
		this.petFormAddressEditText.setText(this.model.getAddress());
		this.petFormCellphoneEditText.setText(String.valueOf(this.model.getCell_phone()));
		this.petFormLandphoneEditText.setText(String.valueOf(this.model.getLand_phone()));
		this.petFormHomephoneEditText.setText(String.valueOf(this.model.getHome_phone()));
		this.petFormFaxEditText.setText(String.valueOf(this.model.getFax_phone()));

		Bitmap bitmap = this.model.getPhotoBitmap();
		if(bitmap != null) {
			this.setPhotoBitmap(this.model.getPhotoBitmap());
		}
	}

	public void onSaveClick(View view) {
		if(!this.collect_inputs()) {
			return;
		}

		this.model.loadFromBundle(this.inputBundle);

		this.model.save();

		this.finish();
	}

	public void onCancelClick(View view) {
		this.finish();
	}

	private boolean collect_inputs() {
		this.inputBundle = new Bundle();

		this.inputBundle.putString(DatabaseHandler.PET_PET_NAME, this.petFormPetNameEditText.getText().toString());
		if(this.inputBundle.getString(DatabaseHandler.PET_PET_NAME).length() == 0) {
			Toast.makeText(this, R.string.form_error_pet_name, Toast.LENGTH_LONG).show();
			return false;
		}

		this.inputBundle.putInt(DatabaseHandler.PET_GENDER, this.getGender());
		if(this.inputBundle.getInt(DatabaseHandler.PET_GENDER) == -1) {
			Toast.makeText(this, R.string.form_error_gender, Toast.LENGTH_LONG).show();
			return false;
		}

		this.inputBundle.putString(DatabaseHandler.PET_BIRTHDAY, this.petFormBirthdayEditText.getText().toString());

		this.inputBundle.putString(DatabaseHandler.PET_KIND, this.petFormKindEditText.getText().toString());

		this.inputBundle.putString(DatabaseHandler.PET_BREED, this.petFormBreedEditText.getText().toString());

		try {
			String chip_codeString = this.petFormChipCodeEditText.getText().toString();
			if(chip_codeString.length() > 0) { // null is allowed
				this.inputBundle.putLong(DatabaseHandler.PET_CHIP_CODE, Long.parseLong(chip_codeString));
			}
		}
		catch (NumberFormatException e) {
			Toast.makeText(this, R.string.form_error_chip_code, Toast.LENGTH_LONG).show();
			return false;
		}

		this.inputBundle.putString("vet_name", this.petFormVetNameEditText.getText().toString());

		this.inputBundle.putString("", this.petFormAddressEditText.getText().toString());

		try {
			String cell_phoneString = this.petFormCellphoneEditText.getText().toString();
			if(cell_phoneString.length() > 0) { // null is allowed
				this.inputBundle.putLong(DatabaseHandler.PET_CELL_PHONE, Long.parseLong(cell_phoneString));
			}

			String land_phoneString = this.petFormLandphoneEditText.getText().toString();
			if(land_phoneString.length() > 0) { // null is allowed
				this.inputBundle.putLong(DatabaseHandler.PET_LAND_PHONE, Long.parseLong(land_phoneString));
			}

			String home_phoneString = this.petFormHomephoneEditText.getText().toString();
			if(home_phoneString.length() > 0) { // null is allowed
				this.inputBundle.putLong(DatabaseHandler.PET_HOME_PHONE, Long.parseLong(home_phoneString));
			}

			String fax_phoneString = this.petFormFaxEditText.getText().toString();
			if(fax_phoneString.length() > 0) { // null is allowed
				this.inputBundle.putLong(DatabaseHandler.PET_FAX_PHONE, Long.parseLong(fax_phoneString));
			}
		}
		catch (NumberFormatException e) {
			Toast.makeText(this, R.string.form_error_phones, Toast.LENGTH_LONG).show();
			return false;
		}

		this.model.setPhotoBitmap(this.photoBitmap);

		return true;
	}

	private int getGender() {
		int radio_button_id = this.genderRadioGroup.getCheckedRadioButtonId();
		if(radio_button_id == R.id.maleRadioButton) {
			return 0;
		}
		else if(radio_button_id == R.id.femaleRadioButton) {
			return 1;
		}
		return -1;
	}

	private void setGender(int gender) {
		if(gender == 0) {
			this.maleRadioButton.setChecked(true);
		}
		else if(gender == 1) {
			this.femaleRadioButton.setChecked(true);
		}
	}

	private void initViews() {
		this.setContentView(R.layout.activity_pet_form);

		this.titleTextView = (TextView) this.findViewById(R.id.titleTextView);
		this.titleTextView.setText(PetFormActivity.DEFAULT_TITLE_STRING);

		this.petFormPetNameEditText = (EditText) this.findViewById(R.id.petFormPetNameEditText);
		this.genderRadioGroup = (RadioGroup) this.findViewById(R.id.genderRadioGroup);
		this.petFormBirthdayEditText = (EditText) this.findViewById(R.id.petFormBirthdayEditText);
		this.petFormKindEditText = (EditText) this.findViewById(R.id.petFormKindEditText);
		this.petFormBreedEditText = (EditText) this.findViewById(R.id.petFormBreedEditText);
		this.petFormChipCodeEditText = (EditText) this.findViewById(R.id.petFormChipCodeEditText);
		this.petFormVetNameEditText = (EditText) this.findViewById(R.id.petFormVetNameEditText);
		this.petFormAddressEditText = (EditText) this.findViewById(R.id.petFormAddressEditText);
		this.petFormCellphoneEditText = (EditText) this.findViewById(R.id.petFormCellphoneEditText);
		this.petFormLandphoneEditText = (EditText) this.findViewById(R.id.petFormLandphoneEditText);
		this.petFormHomephoneEditText = (EditText) this.findViewById(R.id.petFormHomephoneEditText);
		this.petFormFaxEditText = (EditText) this.findViewById(R.id.petFormFaxEditText);
		this.petFormPhotoImageView = (ImageView) this.findViewById(R.id.petFormPhotoImageView);

		this.maleRadioButton = (RadioButton) this.findViewById(R.id.maleRadioButton);
		this.femaleRadioButton = (RadioButton) this.findViewById(R.id.femaleRadioButton);
	}
}
