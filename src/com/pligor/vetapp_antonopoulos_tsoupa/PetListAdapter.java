package com.pligor.vetapp_antonopoulos_tsoupa;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pinapps.vetapp.R;
import com.pligor.pet_crud.PetModel;

public class PetListAdapter extends BaseAdapter {
	private Context context;
	private LayoutInflater layoutInflater;

	private List<PetModel> items = new ArrayList<PetModel>();

	/**
	 * write only
	 * 
	 * @param items
	 */
	public void setItems(List<PetModel> items) {
		this.items = items;
		this.notifyDataSetChanged(); // to notify the list view that the data has changed and to refresh
	}

	public PetListAdapter(Context ctx) {
		this.context = ctx;
		this.layoutInflater = LayoutInflater.from(this.context);
	}

	public int getCount() {
		if(this.items == null) {
			return 0;
		}
		else {
			return items.size();
		}
	}

	public Object getItem(int position) {
		if(this.items == null) {
			return null;
		}
		else {
			return this.items.get(position);
		}
	}

	/**
	 * @param position
	 * @return
	 */
	public long getItemId(int position) {
		return this.items.get(position).getId();
	}

	/**
	 * Tutorial: http://xjaphx.wordpress.com/2011/06/16/viewholder-pattern-caching-view-efficiently/ By the above tutorial we are NOT caching the values! We are
	 * only caching the TextView, ImageView and other variables that play role for the structure of the view. But NOT the actual values like the actual string
	 * in the TextView! Scrolling is very fast!!
	 */
	public View getView(int position, View convertView, ViewGroup parent) {

		if(convertView == null) {
			// no worries to attach to a parent. this job is done from the adapter
			convertView = this.layoutInflater.inflate(R.layout.row_pet_view, null);

			ViewHolder holder = new ViewHolder();
			holder.petShortImageView = (ImageView) convertView.findViewById(R.id.petShortImageView);
			holder.petShortNameTextView = (TextView) convertView.findViewById(R.id.petShortNameTextView);
			holder.petShortGenderBreedTextView = (TextView) convertView.findViewById(R.id.petShortGenderBreedTextView);

			// tags can be used to save data within a view! Hackerish!
			convertView.setTag(holder); // so here we save the view holder inside the view as raw data!
		}

		PetModel model = this.items.get(position);
		if(model != null) {
			ViewHolder holder = (ViewHolder) convertView.getTag();

			Bitmap bitmap = model.getPhotoBitmap();
			if(bitmap == null) {
				holder.petShortImageView.setImageResource(R.drawable.no_pet);
			}
			else {
				holder.petShortImageView.setImageBitmap(bitmap);
			}

			holder.petShortNameTextView.setText(model.getPet_name());
			holder.petShortGenderBreedTextView.setText(model.getGenderBreed());
		}

		// alternate color on lines
		// String colorString = RssListActivity.EVEN_COLOR_STRING;
		// if((position % 2) == 1) {
		// colorString = RssListActivity.ODD_COLOR_STRING;
		// }
		// convertView.setBackgroundColor(Color.parseColor(colorString));

		return convertView;
	}

	/**
	 * A static nested class (like the one below) can only play ball when the class is static. While a non-static nested class can play ball when we have an
	 * object of this class. This means have also access to its non-static methods, non static attributes In other words means that the "static" allows it to
	 * play ball only as static. forget about object occasions
	 */
	static class ViewHolder {
		ImageView petShortImageView;
		TextView petShortNameTextView;
		TextView petShortGenderBreedTextView;
	}
}
