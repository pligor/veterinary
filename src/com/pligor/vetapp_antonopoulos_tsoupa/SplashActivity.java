package com.pligor.vetapp_antonopoulos_tsoupa;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.pinapps.vetapp.R;

public class SplashActivity extends Activity {

	private static final int DELAY_MSEC = 3000;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.initViews();

		Thread timer = new Thread() {

			@Override
			public void run() {
				super.run();

				try {
					Thread.sleep(SplashActivity.DELAY_MSEC);
				}
				catch (InterruptedException e) {
					e.printStackTrace();
				}
				finally {
					Intent intent = new Intent(SplashActivity.this, AboutActivity.class);
					SplashActivity.this.startActivity(intent);
				}
			}
		};

		timer.start();
	}

	private void initViews() {
		this.setContentView(R.layout.activity_splash);
	}
}
