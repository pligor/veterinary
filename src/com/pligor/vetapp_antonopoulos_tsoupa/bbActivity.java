package com.pligor.vetapp_antonopoulos_tsoupa;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import com.pinapps.vetapp.R;

public class bbActivity extends Activity {

	private static enum KIND {
		ABOUT, APPOINTMENT, RSS, PET_INFO,
	};

	Button aboutButton;
	Button appointmentButton;
	Button rssButton;
	Button petInfoButton;

	@Override
	protected void onStart() {
		super.onStart();

		this.initButtons();

		this.initButtonsListeners();

		this.highlightBottomButton();
	}

	private void initButtons() {
		this.aboutButton = (Button) this.findViewById(R.id.aboutButton);
		this.appointmentButton = (Button) this.findViewById(R.id.appointmentButton);
		this.rssButton = (Button) this.findViewById(R.id.rssButton);
		this.petInfoButton = (Button) this.findViewById(R.id.petInfoButton);
	}

	private void initButtonsListeners() {
		this.aboutButton.setOnClickListener(this.bottomButtonListener);
		this.appointmentButton.setOnClickListener(this.bottomButtonListener);
		this.rssButton.setOnClickListener(this.bottomButtonListener);
		this.petInfoButton.setOnClickListener(this.bottomButtonListener);
	}

	private OnClickListener bottomButtonListener = new OnClickListener() {

		public void onClick(View v) {
			int view_id = v.getId();

			Intent intent = null;

			Class targetClass = null;

			if(view_id == R.id.aboutButton) {
				targetClass = AboutActivity.class;
			}
			else if(view_id == R.id.appointmentButton) {
				targetClass = AppointmentFormActivity.class;
			}
			else if(view_id == R.id.rssButton) {
				targetClass = RssLoadingActivity.class;
			}
			else if(view_id == R.id.petInfoButton) {
				targetClass = PetListActivity.class;
			}

			intent = new Intent(bbActivity.this, targetClass);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			bbActivity.this.startActivity(intent);
			bbActivity.this.finish();
		}
	};

	protected void highlightBottomButton() {
		String className = this.getClass().getSimpleName(); // so here we get the correct name of the class

		int string_id = this.getResources().getIdentifier(className, "string", this.getPackageName());
		String kindString = this.getResources().getString(string_id);
		KIND kind = KIND.valueOf(kindString);

		Button curButton = null;

		int drawable_id = 0;

		if(kind == KIND.ABOUT) {
			curButton = this.aboutButton;
			drawable_id = R.drawable.info_highlight;
		}
		else if(kind == KIND.APPOINTMENT) {
			curButton = this.appointmentButton;
			drawable_id = R.drawable.date_highlight;
		}
		else if(kind == KIND.RSS) {
			curButton = this.rssButton;
			drawable_id = R.drawable.rss_highlight;
		}
		else if(kind == KIND.PET_INFO) {
			curButton = this.petInfoButton;
			drawable_id = R.drawable.pet_info_highlight;
		}

		curButton.setOnClickListener(null); // disable clickability for highlighted button
		curButton.setCompoundDrawablesWithIntrinsicBounds(0, drawable_id, 0, 0);
		curButton.setBackgroundResource(R.drawable.bottom_button_pressed);
	}
}
