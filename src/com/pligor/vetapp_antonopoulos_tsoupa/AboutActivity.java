package com.pligor.vetapp_antonopoulos_tsoupa;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.pinapps.vetapp.R;
import com.pligor.generic.StaticTools;

/**
 * Helpful generator: http://gmaps-samples.googlecode.com/svn/trunk/simplewizard/makestaticmap.html
 * 
 * @author pligor
 * 
 */
public class AboutActivity extends bbActivity implements OnClickListener {
	// Irroon 107, Krhths Kalamata: 37.027996,22.121698
	static final float first_lat = (float) 37.027996;
	static final float first_lon = (float) 22.121698;

	// Stoupa Manis: 36.844478,22.262236
	static final float second_lat = (float) 36.844478;
	static final float second_lon = (float) 22.262236;

	public static final long LANDPHONE = Long.parseLong("2721028674");
	static final long CELLPHONE = Long.parseLong("6945410085");

	static final String FB_GROUP = "http://www.facebook.com/groups/59297016296/";
	static final String TWITTER_ACCOUNT = "http://twitter.com/vetgr";

	ImageView aboutFirstStaticImageView;
	ImageView aboutSecondStaticImageView;

	AlertDialog callLandDialog;
	AlertDialog callCellDialog;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// must be executed before setting content layout
		// this.requestWindowFeature(Window.FEATURE_NO_TITLE); //no need since it is set on manifest

		this.initViews();

		this.initGoogleStaticMaps();

		this.callLandDialog = this.initCallDialog(AboutActivity.LANDPHONE);
		this.callCellDialog = this.initCallDialog(AboutActivity.CELLPHONE);
	}

	private static String getStaticMapUrl(float lat, float lon) {
		return "http://maps.google.com/maps/api/staticmap?sensor=false&scale=1&zoom=14&center=" + lat + "," + lon + "&markers=" + lat + "," + lon
				+ "&size=800x600";

	}

	// TODO something inside image loader scales it down!

	// http://gmaps-samples.googlecode.com/svn/trunk/simplewizard/makestaticmap.html
	private void initGoogleStaticMaps() {
		StaticTools.downloadImage(this, AboutActivity.getStaticMapUrl(AboutActivity.first_lat, AboutActivity.first_lon), this.aboutFirstStaticImageView);

		StaticTools.downloadImage(this, AboutActivity.getStaticMapUrl(AboutActivity.second_lat, AboutActivity.second_lon), this.aboutSecondStaticImageView);
	}

	private void initViews() {
		this.setContentView(R.layout.activity_about);
		this.aboutFirstStaticImageView = (ImageView) this.findViewById(R.id.aboutFirstStaticImageView);
		this.aboutSecondStaticImageView = (ImageView) this.findViewById(R.id.aboutSecondStaticImageView);
	}

	private AlertDialog initCallDialog(Long phone) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.call_dialog_title);
		String message = this.getResources().getString(R.string.call_dialog_message) + " " + phone;
		builder.setMessage(message);
		builder.setPositiveButton(R.string.call_dialog_positive_button, this);
		builder.setNegativeButton(R.string.call_dialog_negative_button, null);

		return builder.create();
	}

	public void onClick(DialogInterface dialog, int which) {
		if(which == AlertDialog.BUTTON_POSITIVE) {
			if(dialog == this.callLandDialog) {
				StaticTools.phoneCall(this, AboutActivity.LANDPHONE);
			}
			else if(dialog == this.callCellDialog) {
				StaticTools.phoneCall(this, AboutActivity.CELLPHONE);
			}
		}
	}

	public void onCellphoneClick(View view) {
		this.callCellDialog.show();
	}

	public void onLandphoneClick(View view) {
		this.callLandDialog.show();
	}

	public void onEmailClick(View view) {
		String email = this.getResources().getString(R.string.about_email);
		String subject = "Veterinary app";
		String body = "";
		StaticTools.sendEmail(this, email, subject, body);
	}

	public void onTwitterClick(View view) {
		StaticTools.showWebSite(this, AboutActivity.TWITTER_ACCOUNT);
	}

	public void onFbClick(View view) {
		StaticTools.showWebSite(this, AboutActivity.FB_GROUP);
	}

	public void onOurVetClick(View view) {
		Intent intent = new Intent(this, StaticContentActivity.class);
		this.startActivity(intent);
	}

	public void onUrlClick(View view) {
		// TODO check for connectivity :P
		String url = "http://" + this.getResources().getString(R.string.about_url);
		StaticTools.showWebSite(this, url);
	}

	public void onMapClick(View view) {
		Intent intent = new Intent(this, GoogleMapActivity.class);
		this.startActivity(intent);
	}
}
