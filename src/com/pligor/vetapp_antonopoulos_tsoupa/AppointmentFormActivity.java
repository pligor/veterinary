package com.pligor.vetapp_antonopoulos_tsoupa;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.pinapps.vetapp.R;
import com.pligor.generic.StaticTools;

public class AppointmentFormActivity extends bbActivity implements OnClickListener {

	private static final String FULLNAME = "fullname";
	private static final String TELEPHONE = "telephone";
	private static final String PETNAME = "petname";
	private static final String DATE = "date";
	private static final String TIME = "time";
	private static final String NOTES = "notes";

	private EditText fullnameEditText;
	private EditText telephoneEditText;
	private EditText petnameEditText;
	private EditText dateRequestedEditText;
	private Spinner timeRequestedSpinner;
	private EditText notesEditText;

	private AlertDialog callLandDialog;
	private AlertDialog callCellDialog;

	// REMEMBER: if you parse a date string which provides timezone do NOT enter a Locale which may conflict with the provided time zone!
	public static final SimpleDateFormat dateFormatter = new SimpleDateFormat("d/M/yyyy", new Locale("el"));

	// public static final SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm", new Locale("el"));

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.initViews();

		this.callLandDialog = this.initCallDialog(AboutActivity.LANDPHONE);
		this.callCellDialog = this.initCallDialog(AboutActivity.CELLPHONE);
	}

	private Bundle collectInputs() {
		Bundle inputBundle = new Bundle();
		String errorString = null;

		try {
			String fullname = this.fullnameEditText.getText().toString();
			if(fullname.length() == 0) {
				errorString = "Το Ονοματεπώνυμο δεν επιτρέπεται να είναι κενό";
				throw new Exception(errorString);
			}
			inputBundle.putString(AppointmentFormActivity.FULLNAME, fullname);

			String telephoneString = this.telephoneEditText.getText().toString();
			Long telephone = null;
			try {
				telephone = Long.parseLong(telephoneString);
			}
			catch (NumberFormatException e) {
				// noop
			}
			if((telephoneString.length() != 10) || (telephone == null)) {
				errorString = "Το Τηλέφωνο πρέπει να είναι δεκαεξαψήφιο";
				throw new Exception(errorString);
			}
			inputBundle.putLong(AppointmentFormActivity.TELEPHONE, telephone);

			String petName = this.petnameEditText.getText().toString();
			if(petName.length() == 0) {
				errorString = "Το Όνομα κατοικιδίου δεν επιτρέπεται να είναι κενό";
				throw new Exception(errorString);
			}
			inputBundle.putString(AppointmentFormActivity.PETNAME, petName);

			String dateString = this.dateRequestedEditText.getText().toString();
			Date date = null;
			try {
				date = AppointmentFormActivity.dateFormatter.parse(dateString);
			}
			catch (ParseException e) {
				// noop
			}
			catch (NullPointerException e) {
				// noop
			}

			if(date == null) {
				errorString = "Η Ημερομηνία πρέπει να έχει τη μορφή 16/1/1984";
				throw new Exception(errorString);
			}
			inputBundle.putString(AppointmentFormActivity.DATE, AppointmentFormActivity.dateFormatter.format(date));

			// String timeString = this.timeRequestedEditText.getText().toString();
			// Date time = null;
			// try {
			// time = AppointmentFormActivity.timeFormatter.parse(timeString);
			// }
			// catch (NullPointerException e) {
			// // noop
			// }
			// if(time == null) {
			// errorString = "Η Ώρα πρέπει να έχει τη μορφή 12:00";
			// throw new Exception(errorString);
			// }
			// inputBundle.putString(AppointmentFormActivity.TIME, AppointmentFormActivity.timeFormatter.format(time));

			String timeString = (String) this.timeRequestedSpinner.getSelectedItem();
			inputBundle.putString(AppointmentFormActivity.TIME, timeString);

			String notes = this.notesEditText.getText().toString();
			inputBundle.putString(AppointmentFormActivity.NOTES, notes);

			return inputBundle;
		}
		catch (Exception e) {
			Toast toast = Toast.makeText(this, errorString, Toast.LENGTH_LONG);
			toast.show();
			return null;
		}
	}

	public void onSendFormClick(View view) {
		Bundle inputBundle = this.collectInputs();
		if(inputBundle == null) {
			return;
		}
		String email = this.getResources().getString(R.string.about_email);
		String subject = "Ραντεβού";
		String body = this.getEmailBodyFromBundle(inputBundle);
		StaticTools.sendEmail(this, email, subject, body);
	}

	private String getEmailBodyFromBundle(Bundle bundle) {
		String body = "Επιθυμώ Ραντεβού στο Ιατρείο σας:\n\n";

		Resources resources = this.getResources();

		body += resources.getString(R.string.label_fullname) + ": " + bundle.getString(AppointmentFormActivity.FULLNAME) + "\n";
		body += resources.getString(R.string.label_telephone) + ": " + bundle.getLong(AppointmentFormActivity.TELEPHONE) + "\n";
		body += resources.getString(R.string.label_petname) + ": " + bundle.getString(AppointmentFormActivity.PETNAME) + "\n";
		body += resources.getString(R.string.label_date_requested) + ": " + bundle.getString(AppointmentFormActivity.DATE) + "\n";
		body += resources.getString(R.string.label_time_requested) + ": " + bundle.getString(AppointmentFormActivity.TIME) + "\n";
		body += resources.getString(R.string.label_notes) + ": " + bundle.getString(AppointmentFormActivity.NOTES) + "\n";

		return body;
	}

	private void initViews() {
		this.setContentView(R.layout.activity_appointment_form);
		this.fullnameEditText = (EditText) this.findViewById(R.id.fullnameEditText);
		this.telephoneEditText = (EditText) this.findViewById(R.id.telephoneEditText);
		this.petnameEditText = (EditText) this.findViewById(R.id.petnameEditText);
		this.dateRequestedEditText = (EditText) this.findViewById(R.id.dateRequestedEditText);
		this.timeRequestedSpinner = (Spinner) this.findViewById(R.id.timeRequestedSpinner);
		this.notesEditText = (EditText) this.findViewById(R.id.notesEditText);
	}

	private AlertDialog initCallDialog(Long phone) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.call_dialog_title);
		String message = this.getResources().getString(R.string.call_dialog_message) + " " + phone;
		builder.setMessage(message);
		builder.setPositiveButton(R.string.call_dialog_positive_button, this);
		builder.setNegativeButton(R.string.call_dialog_negative_button, null);

		return builder.create();
	}

	public void onClick(DialogInterface dialog, int which) {
		if(which == AlertDialog.BUTTON_POSITIVE) {
			if(dialog == this.callLandDialog) {
				StaticTools.phoneCall(this, AboutActivity.LANDPHONE);
			}
			else if(dialog == this.callCellDialog) {
				StaticTools.phoneCall(this, AboutActivity.CELLPHONE);
			}
		}
	}

	public void onCellphoneClick(View view) {
		this.callCellDialog.show();
	}

	public void onLandphoneClick(View view) {
		this.callLandDialog.show();
	}
}
