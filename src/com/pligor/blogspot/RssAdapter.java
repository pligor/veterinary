package com.pligor.blogspot;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pligor.generic.StaticTools;
import com.pinapps.vetapp.R;

public class RssAdapter extends BaseAdapter {
	private Context context;
	private LayoutInflater layoutInflater;

	private List<RssModel> items = null;

	/**
	 * write only
	 * 
	 * @param newItems
	 */
	public void setItems(List<RssModel> newItems) {
		this.items = newItems;
	}

	public RssAdapter(Context ctx) {
		this.context = ctx;
		this.layoutInflater = LayoutInflater.from(this.context);
	}

	public int getCount() {
		if(this.items == null) {
			return 0;
		}
		else {
			return items.size();
		}
	}

	public Object getItem(int position) {
		if(this.items == null) {
			return null;
		}
		else {
			return items.get(position);
		}
	}

	/**
	 * NOTE: with our current configuration the position and the id match!
	 * 
	 * @param position
	 * @return
	 */
	public long getItemId(int position) {
		return position;
	}

	/**
	 * Tutorial: http://xjaphx.wordpress.com/2011/06/16/viewholder-pattern-caching-view-efficiently/ By the above tutorial we are NOT caching the values! We are
	 * only caching the TextView, ImageView and other variables that play role for the structure of the view. But NOT the actual values like the actual string
	 * in the TextView! Scrolling is very fast!!
	 */
	public View getView(int position, View convertView, ViewGroup parent) {

		if(convertView == null) {
			// no worries to attach to a parent. this job is done from the adapter
			convertView = this.layoutInflater.inflate(R.layout.rss_line, null);

			ViewHolder holder = new ViewHolder();
			holder.rssLineTitleTextView = (TextView) convertView.findViewById(R.id.rssLineTitleTextView);
			holder.rssLineDateTextView = (TextView) convertView.findViewById(R.id.rssLineDateTextView);
			holder.rssLineImageView = (ImageView) convertView.findViewById(R.id.rssLineImageView);

			// tags can be used to save data within a view! Hackerish :P
			convertView.setTag(holder); // so here we save the view holder inside the view as raw data!
		}

		RssModel item = items.get(position);
		if(item != null) {
			ViewHolder holder = (ViewHolder) convertView.getTag();

			holder.rssLineTitleTextView.setText(item.getTitle());

			holder.rssLineDateTextView.setText(item.getPubDateAsString());

			StaticTools.downloadImage(this.context, item.getThumbnail(), holder.rssLineImageView);
		}

		// alternate color on lines
		String colorString = RssListActivity.EVEN_COLOR_STRING;
		if((position % 2) == 1) {
			colorString = RssListActivity.ODD_COLOR_STRING;
		}
		convertView.setBackgroundColor(Color.parseColor(colorString));

		return convertView;
	}

	/**
	 * A static nested class (like the one below) can only play ball when the class is static. While a non-static nested class can play ball when we have an
	 * object of this class. This means have also access to its non-static methods, non static attributes In other words means that the "static" allows it to
	 * play ball only as static. forget about object occasions
	 */
	static class ViewHolder {
		TextView rssLineTitleTextView;
		TextView rssLineDateTextView;
		ImageView rssLineImageView;
	}
}
