package com.pligor.blogspot;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.util.Log;

/**
 * OK... this is confusing :P String buffers are safe for use by multiple threads. The methods are synchronized where necessary so that all the operations on
 * any particular instance behave as if they occur in some serial order that is consistent with the order of the method calls made by each of the individual
 * threads involved
 * 
 * RSS: http://www.veterinary-gr.blogspot.com/feeds/posts/default?alt=rss
 * 
 * ATOM: http://veterinary-gr.blogspot.gr/atom.xml
 * 
 * @author pligor
 */

public class RssModel {
	// to new Locale("el") kalytera na to vazoume otan theloume na kanoume project thn hmerominia mono
	// diladi diapistothike oti otan pas na kaneis Parse imerominia pou dinei kai timezone enw esy exeis orisei allo locale sou petaei exception

	// Thu, 28 Jun 2012 10:44:00 +0000
	public static final SimpleDateFormat pubDateFormatter = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z");

	// we only need one kind of export format for now so we just leave it as simple as here
	public static final SimpleDateFormat exportFormatter = new SimpleDateFormat("d MMMMM yyyy HH:mm", new Locale("el"));

	public static final String[] attributes = {
			"guid",
			"title",
			"description",
			"link",
			"author",
			"pubDate",
			"thumbnail", // media:thumbnail, contains url, height and width
	// "atom_updated",//atom:updated
	// "thr_total",//thr:total
	};

	/**
	 * @deprecated
	 * @return
	 */
	public static String[] getXmlAttributes() {
		List<String> attrList = new ArrayList<String>(Arrays.asList(RssModel.attributes));
		attrList.remove("thumbnail");
		attrList.add("media:thumbnail");

		String[] strings = new String[attrList.size()];
		int count = 0;
		for (String string : attrList) {
			strings[count++] = string;
		}

		// return (String[]) attrList.toArray(); // giati auto petaei exception?
		return strings;
	}

	public StringBuffer itsGuid = new StringBuffer();
	public StringBuffer itsTitle = new StringBuffer();
	public StringBuffer itsDescription = new StringBuffer();
	public StringBuffer itsLink = new StringBuffer();
	public StringBuffer itsAuthor = new StringBuffer();
	public Date itsPubDate = new Date(); // datetime created in format: Thu, 28 Jun 2012 10:44:00 +0000
	public StringBuffer itsThumbnail = new StringBuffer(); // image link

	// myxercode sample:2003092 probably some code from myxer :P
	// profileid sample: 20858072 the id of the artist. Useful only if we are going to save the profiles inside our own database
	// reviewed sample: 1 probably if has been reviewed or not
	// myxerselect sample: 0 probably if it is recommended from myxer

	private Context context;

	public RssModel(Context ctx) {
		this.context = ctx;
	}

	public String getGuid() {
		return itsGuid.toString();
	}

	public String getTitle() {
		return itsTitle.toString();
	}

	public String getDescription() {
		return itsDescription.toString();
	}

	public String getLink() {
		return itsLink.toString();
	}

	public String getAuthor() {
		return itsAuthor.toString();
	}

	public String getThumbnail() {
		return itsThumbnail.toString();
	}

	public void setGuid(String string) {
		this.itsGuid.setLength(0); // clear before appending
		this.itsGuid.append(string);
	}

	public void setTitle(String string) {
		this.itsTitle.setLength(0); // clear before appending
		this.itsTitle.append(string);
	}

	public void setDescription(String string) {
		this.itsDescription.setLength(0); // clear before appending
		this.itsDescription.append(string);
	}

	public void setLink(String string) {
		this.itsLink.setLength(0); // clear before appending
		this.itsLink.append(string);
	}

	public void setAuthor(String string) {
		this.itsAuthor.setLength(0); // clear before appending
		this.itsAuthor.append(string);
	}

	public void setThumbnail(String string) {
		this.itsThumbnail.setLength(0); // clear before appending
		this.itsThumbnail.append(string);
	}

	public Date getPubDate() {
		return itsPubDate;
	}

	public void setPubDate(String string) {
		try {
			this.itsPubDate = RssModel.pubDateFormatter.parse(string);
		}
		catch (ParseException e) {
			e.printStackTrace();
			Log.i("pl", "Parsing the date has failed", e);
		}
	}

	public String getPubDateAsString() {
		return RssModel.exportFormatter.format(this.itsPubDate);
	}

}

/*
 * SAMPLE: <item> <guid isPermaLink="false">tag:blogger.com,1999:blog-879020333024810749.post-2585084744783393513</guid> <pubDate>Thu, 28 Jun 2012 10:44:00
 * +0000</pubDate> <atom:updated>2012-06-28T03:44:31.525-07:00</atom:updated> <title>Microchip,τι είναι και ποια η χρήση?</title> <description><div dir="ltr"
 * style="text-alig..................................... leftAAAA.blogspot.com' alt='' /></div></description>
 * <link>http://veterinary-gr.blogspot.com/2012/06/microchip.html</link> <author>noreply@blogger.com (veterinary)</author> <media:thumbnail
 * url="http://1.bp.blogspot.com/-NTxElKpi_YQ/T-w1hRL9WgI/AAAAAAAAACQ/ORAiFU88ySI/s72-c/dog_identification.jpg" height="72" width="72"/>
 * <thr:total>0</thr:total> </item>
 */
