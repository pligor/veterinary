package com.pligor.blogspot;

import java.util.List;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.pligor.generic.StaticTools;
import com.pinapps.vetapp.R;
import com.pligor.vetapp_antonopoulos_tsoupa.bbActivity;

public class RssItemActivity extends bbActivity implements OnClickListener {
	public static List<RssModel> items;

	public static final String EXTRA_ITEM_ID_STRING = "item_id";

	public int item_id;

	private TextView titleTextView;
	private TextView dateTextView;
	private ImageView imageView;
	private ImageButton downImageButton;
	private ImageButton upImageButton;
	private WebView descriptionWebView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.initViews();

		this.initListeners();

		this.item_id = (int) this.getIntent().getExtras().getLong(RssItemActivity.EXTRA_ITEM_ID_STRING);

		Log.i("pl", "item id is: " + item_id);

		this.render();
	}

	public void onShareMailClick(View view) {
		RssModel model = this.items.get(this.item_id);
		String email = "";
		String subject = model.getTitle();

		String url = this.getResources().getString(R.string.about_url);

		String body = "<a href=\"http://" + url + "\">";
		body += "Από Veterinary Application:" + "</a>";
		body += "<br/>";
		body += "http://" + url;
		body += "<br/>";
		body += model.getDescription();

		StaticTools.sendHtmlEmail(this, email, subject, body);
	}

	public void onHomeClick(View view) {
		this.finish();
	}

	private void render() {
		RssModel model = this.items.get(this.item_id);

		this.titleTextView.setText(model.getTitle());

		this.dateTextView.setText(model.getPubDateAsString());

		StaticTools.downloadImage(this, model.getThumbnail(), this.imageView);

		// loadDataWithBaseURL much better than loadData: http://stackoverflow.com/questions/7625946/encoding-issue-with-webviews-loaddata
		// this.descriptionWebView.loadData(model.getDescription(), "text/html; charset=UTF-8", null);
		this.descriptionWebView.loadDataWithBaseURL(model.getLink(), model.getDescription(), "text/html", "UTF-8", "");
	}

	public void onClick(View v) {
		int v_id = v.getId();

		if(v_id == this.upImageButton.getId()) {
			this.affectId(-1);
			this.render();
		}
		else if(v_id == this.downImageButton.getId()) {
			this.affectId(1);
			this.render();
		}
		Log.i("pl", "new id: " + this.item_id);
	}

	private void affectId(int shift) {
		int size = RssItemActivity.items.size();
		this.item_id = ((this.item_id + shift) + size) % size;
	}

	private void initListeners() {
		this.upImageButton.setOnClickListener(this);
		this.downImageButton.setOnClickListener(this);
	}

	private void initViews() {
		this.setContentView(R.layout.activity_rss_item);

		this.titleTextView = (TextView) this.findViewById(R.id.rssItemTitleTextView);
		this.dateTextView = (TextView) this.findViewById(R.id.rssItemDateTextView);
		this.imageView = (ImageView) this.findViewById(R.id.rssItemImageView);

		this.downImageButton = (ImageButton) this.findViewById(R.id.downImageButton);
		this.upImageButton = (ImageButton) this.findViewById(R.id.upImageButton);

		this.descriptionWebView = (WebView) this.findViewById(R.id.rssItemDescriptionWebView);
	}
}
