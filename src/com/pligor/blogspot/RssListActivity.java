package com.pligor.blogspot;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.pligor.generic.StaticTools;
import com.pligor.vetapp_antonopoulos_tsoupa.AboutActivity;
import com.pinapps.vetapp.R;
import com.pligor.vetapp_antonopoulos_tsoupa.RssLoadingActivity;
import com.pligor.vetapp_antonopoulos_tsoupa.bbActivity;

public class RssListActivity extends bbActivity implements OnItemClickListener, OnClickListener {

	private ListView itemListView;

	private List<RssModel> items;

	private RssAdapter rssAdapter;

	public static final String EXTRA_FINISH_LOADING = "finish_rss_loading_activity";

	public final static String EVEN_COLOR_STRING = "#FFF0F0F0";
	public final static String ODD_COLOR_STRING = "#FFFFFFFF";

	private AlertDialog callDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.setResult(Activity.RESULT_OK); // Initialize result

		this.initViews();

		this.initListeners();

		this.parseXml();

		this.initListView();

		this.initCallDialog();
	}

	private void initCallDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.call_dialog_title);
		String message = this.getResources().getString(R.string.call_dialog_message) + " " + AboutActivity.LANDPHONE;
		builder.setMessage(message);
		builder.setPositiveButton(R.string.call_dialog_positive_button, this);
		builder.setNegativeButton(R.string.call_dialog_negative_button, null);

		this.callDialog = builder.create();
	}

	private void initListView() {
		this.rssAdapter = new RssAdapter(this);
		this.rssAdapter.setItems(this.items);
		this.itemListView.setAdapter(this.rssAdapter);
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();

		if(this.items == null) {
			this.noItems();
		}
		else if(this.items.isEmpty()) {
			this.noItems();
		}
	}

	private void noItems() {
		Toast toast = Toast.makeText(this.getBaseContext(), "no articles where found", Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();

		this.setResult(Activity.RESULT_CANCELED);
		this.finish();

		// Intent intent = new Intent(this, MainActivity.class);
		// intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // clear all activities
		// this.startActivity(intent);
	}

	/**
	 * TODO HEAVY STUFF, do inside async task later
	 */
	private void parseXml() {
		String xml = this.getIntent().getExtras().getString(RssLoadingActivity.EXTRA_RSS_STRING);
		this.items = StaticTools.startXMLParsing(xml, new XMLParser(this));
		// Log.i("pl", "item size: " + items.size());
	}

	private void initViews() {
		this.setContentView(R.layout.activity_rss_list);
		this.itemListView = (ListView) this.findViewById(R.id.itemListView);
	}

	private void initListeners() {
		this.itemListView.setOnItemClickListener(this);
	}

	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		// NOTE: with our current configuration the position and the id match!
		// In general we will want to have the id of the item

		// pass by the static attribute pattern recommended by polyxronis
		// otherwise must create database. passing single item information is not enough since we have up and down buttons
		RssItemActivity.items = this.items;

		Intent intent = new Intent(this, RssItemActivity.class);
		intent.putExtra(RssItemActivity.EXTRA_ITEM_ID_STRING, id);
		this.startActivity(intent);
	}

	public void onPhoneImageButtonClick(View view) {
		this.callDialog.show();
	}

	public void onMailImageButtonClick(View view) {
		String email = this.getResources().getString(R.string.about_email);
		String subject = "Veterinary App";
		String body = "";
		StaticTools.sendEmail(this, email, subject, body);
	}

	public void onClick(DialogInterface dialog, int which) {
		// we do not check which dialog this is but never mind we only got one dialog
		if(which == AlertDialog.BUTTON_POSITIVE) {
			StaticTools.phoneCall(this, AboutActivity.LANDPHONE);
		}
	}
}
