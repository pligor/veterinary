package com.pligor.blogspot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.content.Context;
import android.util.Log;

/**
 * alli magkia kai auth. Sta RSS items to <media:thumbnail> diavazetai apla ws thumbnail kai oxi ws media:thumbnail
 * 
 * @author pligor
 * 
 */
public class XMLParser extends DefaultHandler {

	private static final String ITEM_STRING = "item";

	private List<RssModel> itemList = new ArrayList<RssModel>();

	private RssModel model = null;

	private HashMap<String, Boolean> check = new HashMap<String, Boolean>();

	private Context context;

	public XMLParser(Context ctx) {
		this.context = ctx;
		this.clearCheck();
	}

	private void clearCheck() {
		check.put(XMLParser.ITEM_STRING, false);

		String[] attributes = RssModel.attributes;
		for (int i = 0; i < attributes.length; i++) {
			String attribute = attributes[i];
			check.put(attribute, false);
		}
	}

	public List<RssModel> getList() {
		return itemList;
	}

	/** Gets be called on opening tags like: <tag> **/
	@Override
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
		if(localName.contentEquals(XMLParser.ITEM_STRING)) {
			this.model = new RssModel(this.context);
		}

		this.check.put(localName, true);

		if(!this.check.get(XMLParser.ITEM_STRING)) { // if you are not inside an item, skip
			return;
		}

		if(localName.contentEquals("thumbnail")) {
			String url = atts.getValue("url");
			// Log.i("pl", "the url is: " + url);
			this.model.itsThumbnail.append(url);
		}
	}

	/**
	 *
	 */
	@Override
	public void characters(char ch[], int start, int length) {
		String string = new String(ch, start, length);

		if(!this.check.get(XMLParser.ITEM_STRING)) { // if you are not inside an item, skip
			return;
		}

		if(this.check.get("guid")) {
			this.model.itsGuid.append(string);
		}
		else if(this.check.get("title")) {
			this.model.itsTitle.append(string);
		}
		else if(this.check.get("description")) {
			this.model.itsDescription.append(string);
		}
		else if(this.check.get("link")) {
			this.model.itsLink.append(string);
		}
		else if(this.check.get("author")) {
			this.model.itsAuthor.append(string);
		}
		else if(this.check.get("pubDate")) {
			this.model.setPubDate(string);
		}
		else if(this.check.get("thumbnail")) {
			Log.i("pl", "ERROR! normally it should not enter in here");
		}
	}

	/**
	 * Gets be called on closing tags like: </tag>
	 */
	@Override
	public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
		if(localName.contentEquals(XMLParser.ITEM_STRING)) {
			if(this.model != null) {
				itemList.add(this.model);
			}
		}

		this.check.put(localName, false);
	}
}
