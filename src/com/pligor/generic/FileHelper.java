package com.pligor.generic;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;

public class FileHelper {
	public static final String TEMP_FOLDER = "tmp";
	private final static int IMAGE_COMPRESSION_QUALITY = 90;

	/**
	 * 
	 * @param fullpath
	 * @return
	 */
	public static boolean file_exists(String fullpath) {
		return (new File(fullpath)).exists();
	}

	/**
	 * recursive
	 * 
	 * @param subpath
	 */
	public static boolean mkdir_to_sd(String subpath) {
		File path = new File(subpath);
		return path.mkdirs();
	}

	/**
	 * TESTED OK
	 * 
	 * @param path
	 * @return
	 */
	private static String put_trailing_slash(String path) {
		if(path.substring(path.length() - 1).contentEquals("/")) {
			return path;
		}
		return path + "/";
	}

	public static void file_put_contents_to_sd(String subpath, String filename, String data) {
		FileOutputStream fout = FileHelper.prepareOutputStream(subpath, filename);

		try {
			fout.write(data.getBytes());
			fout.flush();
			fout.close();
		}
		catch (IOException e) {
			e.printStackTrace();
			Log.i("pl", "io exception ??", e);
		}
	}

	public static void file_put_bitmap_to_sd(String subpath, String filename, Bitmap bitmap) {
		FileOutputStream fout = FileHelper.prepareOutputStream(subpath, filename);

		if(bitmap == null) {
			Log.i("pl", "bitmap is null");
			return;
		}
		bitmap.compress(Bitmap.CompressFormat.PNG, FileHelper.IMAGE_COMPRESSION_QUALITY, fout);

		try {
			fout.flush();
			fout.close();
		}
		catch (IOException e) {
			e.printStackTrace();
			Log.i("pl", "io exception ??", e);
		}
	}

	private static FileOutputStream prepareOutputStream(String subpath, String filename) {
		String path = Environment.getExternalStorageDirectory() + "/" + subpath;

		FileHelper.mkdir_to_sd(path); // if it exists this will return false but it is ok

		File file = new File(path, filename);

		FileOutputStream fout = null;
		try {
			fout = new FileOutputStream(file);
		}
		catch (FileNotFoundException e1) {
			e1.printStackTrace();
			Log.i("pl", "file not found ??", e1);
		}

		return fout;
	}
}
