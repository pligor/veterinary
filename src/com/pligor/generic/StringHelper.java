package com.pligor.generic;

public class StringHelper {

	/**
	 * TESTED OK http://stackoverflow.com/questions/1184176/how-can-i-safely-encode-a-string-in-java-to-use-as-a-filename
	 * 
	 * @param string
	 * @return
	 */
	public static String onlyLettersNumbersUnderscores(String string) {
		return string.replaceAll("\\W+", "");
	}
}
