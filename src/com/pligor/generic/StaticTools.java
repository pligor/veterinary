package com.pligor.generic;

import java.io.Reader;
import java.io.StringReader;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.text.Html;
import android.util.Log;
import android.widget.ImageView;

import com.fedorvlasov.lazylist.ImageLoader;
import com.pligor.blogspot.XMLParser;

public class StaticTools {

	public static void phoneCall(Activity activity, Long phoneNumber) {
		String uriString = "tel:" + phoneNumber;
		Intent intent = new Intent(Intent.ACTION_CALL);
		Uri phoneUri = Uri.parse(uriString);
		intent.setData(phoneUri);
		activity.startActivity(intent);
	}

	public static void sendHtmlEmail(Activity activity, String email, String subject, String body) {
		String[] emails = {
			email
		};

		// http://stackoverflow.com/questions/2007540/how-to-send-html-email
		// Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:"));
		Intent intent = new Intent(android.content.Intent.ACTION_SEND);
		intent.putExtra(android.content.Intent.EXTRA_EMAIL, emails);
		intent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
		intent.setType("text/html");
		intent.putExtra(android.content.Intent.EXTRA_TEXT, Html.fromHtml(body));

		activity.startActivity(intent);
	}

	/**
	 * Generally is supported the send to multiple email. Here we support only one
	 */
	public static void sendEmail(Activity activity, String email, String subject, String body) {
		String[] emails = {
			email
		};

		Intent intent = new Intent(android.content.Intent.ACTION_SEND);
		intent.putExtra(android.content.Intent.EXTRA_EMAIL, emails);
		intent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
		intent.setType("text/plain");
		intent.putExtra(android.content.Intent.EXTRA_TEXT, body);

		activity.startActivity(intent);
	}

	public static boolean isConnected(final Activity act) {
		ConnectivityManager connec = (ConnectivityManager) act.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = null;
		try {
			netInfo = connec.getActiveNetworkInfo();
		}
		catch (NullPointerException e) {
			return false;
		}
		if(netInfo != null) {
			if(netInfo.getState() == NetworkInfo.State.CONNECTED) {
				return true;
			}
			else {
				return false;
			}
		}
		connec = null;
		netInfo = null;
		return false;
	}

	public static final boolean isOn3G(Context ctx) {
		NetworkInfo info = (NetworkInfo) ((ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
		if(info != null && info.getTypeName().toLowerCase().equals("mobile")) return true;
		else return false;
	}

	private static ImageLoader imageLoader; // this imageloader outside is also a performance issue

	public static void downloadImage(Context ctx, String url, ImageView imageview) {
		if(StaticTools.imageLoader == null) {
			StaticTools.imageLoader = new ImageLoader(ctx);
		}
		StaticTools.imageLoader.DisplayImage(url, imageview);
	}

	// http://stackoverflow.com/questions/6900452/android-starting-web-browser-from-code
	public static void showWebSite(Activity activity, String url) {
		Intent webIntent = new Intent(Intent.ACTION_VIEW);
		webIntent.setData(Uri.parse(url));
		try {
			activity.startActivity(webIntent);
		}
		catch (ActivityNotFoundException e) {
			Log.i("pl", "the given url is NOT prepended with http://");
		}
	}

	public static boolean isSDcardAvailable() {
		return android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
	}

	public static final List startXMLParsing(final String xml, final XMLParser pListParser) {
		try {
			SAXParserFactory spf = SAXParserFactory.newInstance();
			SAXParser sp = spf.newSAXParser();
			XMLReader xr = sp.getXMLReader();

			xr.setContentHandler(pListParser);

			InputSource inStream = new InputSource();

			Reader characterStreamReader = new StringReader(xml);

			inStream.setCharacterStream(characterStreamReader);

			xr.parse(inStream);

			return pListParser.getList();
		}
		catch (Exception e) {
			e.printStackTrace();
			Log.i("pl", "parsing failed", e);
			return null;
		}
	}
}
