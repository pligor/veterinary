package com.pligor.generic;

public class MapHelper {
	public static int degreesToInt(double degrees) {
		return (int) (degrees * 1E6);
	}

	public static double degreesToDouble(int degrees) {
		return (double) degrees / (double) 1E6;
	}
}
