
package com.pligor.generic;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class InternetFetcher {

    public String getData(String url) {

        URLConnection conn;
        StringBuffer sb1 = new StringBuffer();
        try {
            URL updateURL = new URL(url);
            conn = updateURL.openConnection();
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            String s = "";
            while ((s = rd.readLine()) != null) {
                sb1.append(s);
            }
            return sb1.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}