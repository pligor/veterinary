package com.pligor.pet_crud;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;

import com.pligor.generic.FileHelper;
import com.pligor.generic.StringHelper;
import com.pinapps.vetapp.R;
import com.pligor.vetapp_antonopoulos_tsoupa.Veterinary;

/**
 * 
 * @author pligor
 * 
 */
public class PetModel {
	private Integer id = null; // initialize with null so we know its a new model
	private String pet_name;
	private Integer gender;
	private String birthday;
	private String kind;
	private String breed;
	private Long chip_code;
	private String vet_name;
	private String address;
	private Long cell_phone;
	private Long land_phone;
	private Long home_phone;
	private Long fax_phone;
	private String photo;

	private Bitmap photoBitmap = null;

	private static final String photoFolder = "DCIM/veterinary";

	private String getPhotoFullpath() {
		return Environment.getExternalStorageDirectory() + "/" + "DCIM/veterinary" + "/" + this.photo;
	}

	public PetModel() {

	}

	public PetModel(Cursor cursor) {
		this.loadFromCursor(cursor);
	}

	private void photoFilenameFromPetName() {
		this.photo = StringHelper.onlyLettersNumbersUnderscores(this.pet_name) + ".png";
	}

	private boolean onBeforeSave() {
		if(this.photoBitmap == null) {
			return true;
		}

		this.photoFilenameFromPetName();

		FileHelper.file_put_bitmap_to_sd(PetModel.photoFolder, this.photo, this.photoBitmap);
		return true;
	}

	public void save() {
		if(!this.onBeforeSave()) {
			return;
		}

		DatabaseHandler dbHandler = new DatabaseHandler(Veterinary.getAppContext());

		if(this.id == null) {
			dbHandler.insert(this);
		}
		else {
			dbHandler.update(this);
		}
	}

	public String getGenderBreed() {
		return this.getGenderAsString() + ", " + this.getBreed();
	}

	public String getGenderAsString() {
		Context context = Veterinary.getAppContext();

		if(this.gender == 0) {
			return context.getResources().getString(R.string.male);
		}
		else if(this.gender == 1) {
			return context.getResources().getString(R.string.female);
		}
		return null;
	}

	public void loadFromBundle(Bundle bundle) {
		this.pet_name = bundle.getString(DatabaseHandler.PET_PET_NAME);
		this.gender = bundle.getInt(DatabaseHandler.PET_GENDER);
		this.birthday = bundle.getString(DatabaseHandler.PET_BIRTHDAY);
		this.kind = bundle.getString(DatabaseHandler.PET_KIND);
		this.breed = bundle.getString(DatabaseHandler.PET_BREED);
		this.chip_code = bundle.getLong(DatabaseHandler.PET_CHIP_CODE);
		this.vet_name = bundle.getString(DatabaseHandler.PET_VET_NAME);
		this.address = bundle.getString(DatabaseHandler.PET_ADDRESS);
		this.cell_phone = bundle.getLong(DatabaseHandler.PET_CELL_PHONE);
		this.land_phone = bundle.getLong(DatabaseHandler.PET_LAND_PHONE);
		this.home_phone = bundle.getLong(DatabaseHandler.PET_HOME_PHONE);
		this.fax_phone = bundle.getLong(DatabaseHandler.PET_FAX_PHONE);
		// this.photo = bundle.getString(DatabaseHandler.PET_PHOTO);
	}

	public void loadFromCursor(Cursor cursor) {
		this.id = cursor.getInt(0);
		this.pet_name = cursor.getString(1);
		this.gender = cursor.getInt(2);
		this.birthday = cursor.getString(3);
		this.kind = cursor.getString(4);
		this.breed = cursor.getString(5);
		this.chip_code = cursor.getLong(6);
		this.vet_name = cursor.getString(7);
		this.address = cursor.getString(8);
		this.cell_phone = cursor.getLong(9);
		this.land_phone = cursor.getLong(10);
		this.home_phone = cursor.getLong(11);
		this.fax_phone = cursor.getLong(12);
		this.photo = cursor.getString(13);

		String fullpath = this.getPhotoFullpath();
		if(FileHelper.file_exists(fullpath)) {
			this.photoBitmap = BitmapFactory.decodeFile(fullpath);
		}
	}

	public ContentValues getContentValues() {
		ContentValues contentValues = new ContentValues();

		contentValues.put(DatabaseHandler.PET_PET_NAME, this.pet_name);
		contentValues.put(DatabaseHandler.PET_GENDER, this.gender);
		contentValues.put(DatabaseHandler.PET_BIRTHDAY, this.birthday);
		contentValues.put(DatabaseHandler.PET_KIND, this.kind);
		contentValues.put(DatabaseHandler.PET_BREED, this.breed);
		contentValues.put(DatabaseHandler.PET_CHIP_CODE, this.chip_code);
		contentValues.put(DatabaseHandler.PET_VET_NAME, this.vet_name);
		contentValues.put(DatabaseHandler.PET_ADDRESS, this.address);
		contentValues.put(DatabaseHandler.PET_CELL_PHONE, this.cell_phone);
		contentValues.put(DatabaseHandler.PET_LAND_PHONE, this.land_phone);
		contentValues.put(DatabaseHandler.PET_HOME_PHONE, this.home_phone);
		contentValues.put(DatabaseHandler.PET_FAX_PHONE, this.fax_phone);
		contentValues.put(DatabaseHandler.PET_PHOTO, this.photo);

		return contentValues;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPet_name() {
		return pet_name;
	}

	public void setPet_name(String pet_name) {
		this.pet_name = pet_name;
	}

	public Integer getGender() {
		return gender;
	}

	public void setGender(Integer gender) {
		this.gender = gender;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public String getBreed() {
		return breed;
	}

	public void setBreed(String breed) {
		this.breed = breed;
	}

	public Long getChip_code() {
		return chip_code;
	}

	public void setChip_code(Long chip_code) {
		this.chip_code = chip_code;
	}

	public String getVet_name() {
		return vet_name;
	}

	public void setVet_name(String vet_name) {
		this.vet_name = vet_name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Long getCell_phone() {
		return cell_phone;
	}

	public void setCell_phone(Long cell_phone) {
		this.cell_phone = cell_phone;
	}

	public Long getLand_phone() {
		return land_phone;
	}

	public void setLand_phone(Long land_phone) {
		this.land_phone = land_phone;
	}

	public Long getHome_phone() {
		return home_phone;
	}

	public void setHome_phone(Long home_phone) {
		this.home_phone = home_phone;
	}

	public Long getFax_phone() {
		return fax_phone;
	}

	public void setFax_phone(Long fax_phone) {
		this.fax_phone = fax_phone;
	}

	public Bitmap getPhotoBitmap() {
		return photoBitmap;
	}

	public void setPhotoBitmap(Bitmap photoBitmap) {
		this.photoBitmap = photoBitmap;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}
}
