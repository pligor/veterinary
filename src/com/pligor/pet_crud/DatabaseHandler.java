package com.pligor.pet_crud;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper {
	public static final String DATABASE_NAME = "vetapp";
	static final int DATABASE_VERSION = 1;

	private Context context;

	public DatabaseHandler(Context context) {
		super(context, DatabaseHandler.DATABASE_NAME, null, DatabaseHandler.DATABASE_VERSION);
		this.context = context;
	}

	public void reset() {
		Boolean deleted = this.context.deleteDatabase(DatabaseHandler.DATABASE_NAME);

		SQLiteDatabase db = this.getReadableDatabase(); // simply to trigger create methods if necessary ;)

		if(deleted) {
			this.populate();
		}

		db.close();
	}

	private void populate() {
		PetModel model = new PetModel();
		model.setPet_name("Bianca");
		model.setGender(1);
		model.setBirthday("18/9/2007");
		model.setKind("Dog");
		model.setBreed("Canadian wolf hound");
		model.setChip_code(Long.parseLong("9876543210"));
		model.setVet_name("Κτηνίατρος Όνομα");
		model.setAddress("Διεύθυνση Κτηνιάτρου");
		model.setCell_phone(Long.parseLong("6982843552"));
		model.setLand_phone((long) 2102430288);
		model.setHome_phone((long) 2102433551);
		model.setFax_phone((long) 2102433552);
		model.setPhoto("Bianca.png");
		this.insert(model);

		model = new PetModel();
		model.setPet_name("miaou");
		model.setGender(0);
		model.setBirthday("1/12/2010");
		model.setKind("cat");
		this.insert(model);
	}

	// Static variables
	public static final String TABLE_PET = "pet";

	public static final String PET_ID = "id";
	public static final String PET_PET_NAME = "pet_name";
	public static final String PET_GENDER = "gender";
	public static final String PET_BIRTHDAY = "birthday";
	public static final String PET_KIND = "kind";
	public static final String PET_BREED = "breed";
	public static final String PET_CHIP_CODE = "chip_code";
	public static final String PET_VET_NAME = "vet_name";
	public static final String PET_ADDRESS = "address";
	public static final String PET_CELL_PHONE = "cell_phone";
	public static final String PET_LAND_PHONE = "land_phone";
	public static final String PET_HOME_PHONE = "home_phone";
	public static final String PET_FAX_PHONE = "fax_phone";
	public static final String PET_PHOTO = "photo";

	@Override
	public void onOpen(SQLiteDatabase db) {
		super.onOpen(db);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// IF YOU FUCKUP THE SQL CREATE STATEMENTS YOU ARE GOING TO NEED context.deleteDatabase("DB_NAME");
		// http://stackoverflow.com/questions/4406067/how-to-delete-sqlite-database-from-android-programmatically
		Log.i("pl", "inside onCreate");

		//@formatter:off
		final String CREATE_TABLE_PET = "CREATE TABLE pet (\n" + 
				"    \"id\" INTEGER PRIMARY KEY AUTOINCREMENT,\n" + 
				"    \"pet_name\" TEXT NOT NULL,\n" + 
				"    \"gender\" INTEGER NOT NULL,\n" + 
				"    \"birthday\" TEXT,\n" + 
				"    \"kind\" TEXT,\n" + 
				"    \"breed\" TEXT,\n" + 
				"    \"chip_code\" INTEGER,\n" + 
				"    \"vet_name\" TEXT,\n" + 
				"    \"address\" TEXT,\n" + 
				"    \"cell_phone\" INTEGER,\n" + 
				"    \"land_phone\" INTEGER,\n" + 
				"    \"home_phone\" INTEGER,\n" + 
				"    \"fax_phone\" INTEGER,\n" + 
				"    \"photo\" TEXT,\n" + 
				"    UNIQUE(\"pet_name\")\n" + 
				" )";
		//@formatter:on

		db.execSQL(CREATE_TABLE_PET);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS); // drop older table if existed
		// this.onCreate(db); // Create tables again
		// just do nothing for the time being
	}

	public void insert(PetModel model) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues contentValues = model.getContentValues();

		db.insert(DatabaseHandler.TABLE_PET, null, contentValues); // Inserting Row

		model.setId(this.getAutoIncrement(db));

		db.close(); // Closing database connection

		Log.i("pl", "inserted model with id: " + model.getId() + " and name: " + model.getPet_name());
	}

	public void update(PetModel model) {
		if(model.getId() == null) {
			Log.i("pl", "you cannot update a new model !!");
			return;
		}
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues contentValues = model.getContentValues();

		String whereClause = DatabaseHandler.PET_ID + " = ?";

		String[] whereArgs = {
			String.valueOf(model.getId()),
		};

		db.update(DatabaseHandler.TABLE_PET, contentValues, whereClause, whereArgs);

		db.close();
		Log.i("pl", "updated model with id: " + model.getId() + " and (new) name: " + model.getPet_name());
	}

	/**
	 * i THINK it works only with a writeable database
	 * 
	 * @param db
	 * @return
	 */
	public int getAutoIncrement(SQLiteDatabase db) {
		String sql = "SELECT last_insert_rowid()";
		Cursor cursor = db.rawQuery(sql, null);

		if(cursor.moveToFirst()) {
			return cursor.getInt(0);
		}

		return 0;
	}

	/**
	 * 
	 * @param model
	 * @return
	 */
	public boolean delete(PetModel model) {
		int pet_id = model.getId();
		return this.delete(pet_id);
	}

	public boolean delete(int pet_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		int rows = db.delete(DatabaseHandler.TABLE_PET, DatabaseHandler.PET_ID + " = ?", new String[] {
			String.valueOf(pet_id)
		});

		db.close();

		Log.i("pl", "deleted model with id: " + pet_id);

		return (rows == 1);
	}

	/**
	 * delete ALL rows
	 */
	public void deleteAll() { // Deleting all rows
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + DatabaseHandler.TABLE_PET);
		db.close();
	}

	public PetModel getById(int pet_id) {
		SQLiteDatabase db = this.getReadableDatabase();

		// Cursor cursor = db.query(TABLE_CONTACTS, new String[] { // KEY_ID, // KEY_NAME, // KEY_PH_NO // }, KEY_ID + "=?", new String[] { //
		// String.valueOf(id)
		// }, null, null, null, null);

		String sql = "SELECT * FROM " + DatabaseHandler.TABLE_PET + " WHERE " + DatabaseHandler.PET_ID + " = ?";

		String[] selectionArgs = {
			String.valueOf(pet_id),
		};

		Cursor cursor = db.rawQuery(sql, selectionArgs);

		if(!cursor.moveToFirst()) {
			db.close();
			return null;
		}

		PetModel model = new PetModel(cursor);

		// logo paraponwn kata to execution, kleinw thn database kai se readable katastasi db
		db.close();
		return model;
	}

	public PetModel getByPetName(String pet_name) {
		SQLiteDatabase db = this.getReadableDatabase();

		String sql = "SELECT * FROM " + DatabaseHandler.TABLE_PET + " WHERE " + DatabaseHandler.PET_PET_NAME + " = ?";

		String[] selectionArgs = {
			pet_name,
		};

		Cursor cursor = db.rawQuery(sql, selectionArgs);

		if(!cursor.moveToFirst()) {
			db.close();
			return null;
		}

		PetModel model = new PetModel(cursor);

		db.close();

		return model;
	}/*
	 * 
	 * /**
	 * 
	 * @return
	 */

	public List getAll() {
		SQLiteDatabase db = this.getReadableDatabase(); // this works fine

		String query = "SELECT * FROM " + DatabaseHandler.TABLE_PET;

		Cursor cursor = db.rawQuery(query, null);

		if(!cursor.moveToFirst()) {
			db.close();
			return null;
		}

		List list = new ArrayList();
		do { // looping through all rows and adding to list
			PetModel model = new PetModel(cursor);
			list.add(model); // Adding contact to list
		}
		while (cursor.moveToNext());

		db.close();

		return list;
	}
}
